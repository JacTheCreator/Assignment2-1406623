
function submit_login() {
    let email = '12345@fake.com';
    let password = '12345678';


    let submitted_email = document.getElementById('email').value;
    let submitted_password = document.getElementById('password').value;

    if (email == submitted_email && password == submitted_password) {
        window.location.href = '/order.html';
    } else {
        document.getElementById('error-msg').style.display = 'block';
    }

    return false
}

function checkForValidCard() {
    let credit_card = document.getElementById("credit-card");
    if (credit_card != null) {
        document.getElementById("order-now").disabled = (credit_card.value.length > 0) ? false : true; 
    }
}

function logout() {
    window.location.href = '/login.html';
}

function reset() {
    document.getElementById("credit-card").value = "";
    document.getElementById("soda").value = "0";
    document.getElementById("beer").value = "0";
    document.getElementById("water").value = "0";

    uncheckedElements('meatless');
    uncheckedElements('meat');
    uncheckedElements('sauce');
    uncheckedElements('size');
    document.getElementsByName('size')[0].checked = true;
    document.getElementsByName('sauce')[0].checked = true;
    document.getElementById("order-now").disabled = true;
}

function findTotalCostOf(elementName) {
    let sumOfValues = 0
    let values = document.getElementsByName(elementName);
    for (let index = 0; index < values.length; index++) {
        if(values[index].checked) {
            sumOfValues += parseInt(values[index].value);
        }
    }
    return sumOfValues;
}

function uncheckedElements(elementName) {
    let values = document.getElementsByName(elementName);
    for (let index = 0; index < values.length; index++) {
        values[index].checked = false;
    }
}

function getSingleToppingCost(elementName) {
    if (elementName == 'meat') {
        let size = findTotalCostOf('size');
        switch (size) {
            case 325:
                return 90;
            
            case 650:
                return 220;
            
            case 1250:
                return 310;
            
            case 1600:
                return 410;
            
            case 1800:
                return 410;
            
            default:
                return 0;
        }
    } else if(elementName == 'meatless') {
        let size = findTotalCostOf('size');
        switch (size) {
            case 325:
                return 90;
            
            case 650:
                return 140;
            
            case 1250:
                return 210;
            
            case 1600:
                return 280;
            
            case 1800:
                return 280;
            
            default:
                return 0;
        }
    }

    return 0;
}

function findTotal() {
    let total = 0;
    
    // Finding the cost depending on the size selected
    let size = findTotalCostOf('size');
    total += (isNaN(size)) ? 0 : size;

    // Finding the cost if a user selects special offer
    let special_offer = findTotalCostOf('offer');
    total += (isNaN(special_offer)) ? 0 : special_offer;

    // Finding the cost if a user selects a sauce
    let sauce = findTotalCostOf('sauce');
    total += (isNaN(sauce)) ? 0 : sauce;

    // Finding the cost if a user selects a sauce
    let meat = findTotalCostOf('meat');
    total += (isNaN(meat)) ? 0 : (meat * getSingleToppingCost('meat'));

    // Finding the cost if a user selects a sauce
    let meatless = findTotalCostOf('meatless');
    total += (isNaN(meatless)) ? 0 : (meatless * getSingleToppingCost('meatless'));

    let beer = parseInt(document.getElementById('beer').value);
    total +=  (isNaN(beer)) ? 0 : (beer * 250);

    let soda = parseInt(document.getElementById('soda').value);
    total +=  (isNaN(soda)) ? 0 : (soda * 100);

    let water = parseInt(document.getElementById('water').value);
    total +=  (isNaN(water)) ? 0 : (water * 50);


    document.getElementById('subtotal').innerText = total;
    document.getElementById('tax').innerText = 0.15 * total;
    document.getElementById('total').innerText = (.15 * total) + total;
}

checkForValidCard();